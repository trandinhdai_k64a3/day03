<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/styless.css">
    <title>Form tân sinh viên</title>
</head>
<body>
    <div class="center">
        <div class="container">
            <div class="border_box">
                <span class="label">Họ và tên</span>
                <input class="input" type="text">
            </div>
            <div class="border_box">
                <span class="label">Giới tính</span>
                <div class="gender">
                    <div class="check">
                        <input type="radio" id="male" name="check_gender" value="Male">
                        <label class="gender_box" for="male">Nam</label>
                    </div>
                    <div class="check">
                        <input type="radio" id="female" name="check_gender" value="Female">
                        <label class="gender_box" for="female">Nữ</label>
                    </div>
                </div>
            </div>
            <div class="border_box">
                <span class="label">Phân khoa</span>
                <select class="input">
                    <option selected="selected">-- Chọn khoa theo học --</option>
                    <?php
                    $department = array("0" => "Toán Cơ Tin", "1" => "Vật lý", "2" => "Hóa học", "3" => "Sinh học");
                    foreach ($department as $key => $value) {
                        echo '<option value="' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="login-btn">
                <button>Đăng nhập</button>
            </div>
        </div>
    </div>
</body>
</html>